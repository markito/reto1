package com.empresaTest.programa.controller;

import com.empresaTest.programa.model.Client;
import com.empresaTest.programa.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class ClientController {

    @Autowired
    ClientService clientService;

    @GetMapping(path = "/query", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> getData(@RequestParam(name = "documentType") String documentType,
                                          @RequestParam(name = "documentNumber") String documentNumber
                                          ){
        return clientService.getData(documentType, documentNumber);
    }

    @PostMapping
    public Client saveClient(@RequestBody  Client client){

        return clientService.saveClient(client);
    }

    @GetMapping("/{id}")
    public Optional<Client> getClientId(@PathVariable Long id){
        return clientService.getClientId(id);
    }

    @GetMapping
    public List<Client> getAllClient(){
        return clientService.getAllClient();
    }

    @DeleteMapping("/{id}")
    public void deleteClient(@PathVariable Long id){
        clientService.deleteClient(id);
    }

    @PutMapping("/{id}")
    public Client updateClient(@PathVariable Long id, @RequestBody  Client client){
        return clientService.updateClient(id, client);
    }

}
