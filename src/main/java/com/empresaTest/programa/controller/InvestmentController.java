package com.empresaTest.programa.controller;

import com.empresaTest.programa.entity.Investment;
import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.entity.Project;
import com.empresaTest.programa.model.InvestmentRequestDto;
import com.empresaTest.programa.service.InvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/investment")
public class InvestmentController {

    @Autowired
    private InvestmentService investmentService;

    // invertir en un proyecto
    @PostMapping
    public ResponseEntity<Investment> saveInvestment(@RequestBody InvestmentRequestDto investment){
        Investment result = investmentService.saveInvestment(investment);
        return ResponseEntity.ok(result);
    }

    // listar inversiones realizadas a un proyecto por ProjectName
    @GetMapping
    public ResponseEntity<List<Investment>> getInvestmentByProjectName(@RequestParam(name = "projectName") String projectName){
        List<Investment> result = investmentService.getAllInvestmentByProjectName(projectName);
        if (result.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(result);
    }

    // listar inversiones realizadas a un proyecto por ProjectId
    @GetMapping
    public ResponseEntity<List<Investment>> getInvestmentByProjectId(@RequestParam(name = "projectId") Long projectId){
        List<Investment> result = investmentService.getAllInvestmentByProjectId(projectId);
        if (result.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(result);
    }
}
