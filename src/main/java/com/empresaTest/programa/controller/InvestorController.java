package com.empresaTest.programa.controller;

import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.model.Client;
import com.empresaTest.programa.service.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/investor")
public class InvestorController {

    @Autowired
    private InvestorService investorService;

    @GetMapping
    public ResponseEntity<Investor> getInvestor(@RequestParam(name = "document") String document){
        Optional<Investor> result = investorService.getInvestor(document);
        if (result.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result.get());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Investor> getInvestorId(@PathVariable Long id){
        Optional<Investor> result = investorService.getInvestorId(id);
        if (result.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result.get());
    }

    @GetMapping
    public ResponseEntity<List<Investor>> getAllInvestor(){
        List<Investor> result = investorService.getAllInvestor();
        if (result.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(result);
    }

    @PostMapping
    public ResponseEntity<Investor> saveInvestor(@RequestBody Investor investor){
        Investor result = investorService.saveInvestor(investor);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Investor>  updateInvestor(@PathVariable Long id, @RequestBody  Investor investor){
        Investor result = investorService.updateInvestor(id, investor);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public void deleteInvestor(@PathVariable Long id){
        investorService.deleteInvestor(id);
    }

}
