package com.empresaTest.programa.controller;

import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.entity.Project;
import com.empresaTest.programa.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping
    public ResponseEntity<Project> getInvestor(@RequestParam(name = "projectName") String projectName){
        Optional<Project> result = projectService.getProject(projectName);
        if (result.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result.get());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Project> getInvestorId(@PathVariable Long id){
        Optional<Project> result = projectService.getProjectId(id);
        if (result.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result.get());
    }

    @GetMapping
    public ResponseEntity<List<Project>> getAllInvestor(){
        List<Project> result = projectService.getAllProject();
        if (result.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(result);
    }

    @PostMapping
    public ResponseEntity<Project> saveInvestor(@RequestBody Project project){
        Project result = projectService.saveProject(project);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Project>  updateProject(@PathVariable Long id, @RequestBody  Project project){
        Project result = projectService.updateProject(id, project);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public void deleteProject(@PathVariable Long id){
        projectService.deleteProject(id);
    }

}
