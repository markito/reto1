package com.empresaTest.programa.repository;

import com.empresaTest.programa.entity.Investment;
import com.empresaTest.programa.entity.Investor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface InvestmentRepository extends JpaRepository<Investment, Long> {

    @Query("SELECT I FROM Investment I WHERE I.project.projectName = ?1")
    List<Investment> findByProyectName(String proyectName);
    @Query("SELECT I FROM Investment I WHERE I.project.id = ?1")
    List<Investment> findByProyectId(Long proyectid);
}
