package com.empresaTest.programa.repository;

import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InvestorRepository extends JpaRepository<Investor, Long> {

    Optional<Investor> findByDocument(String document);
}
