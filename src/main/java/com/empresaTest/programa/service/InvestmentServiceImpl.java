package com.empresaTest.programa.service;

import com.empresaTest.programa.entity.Investment;
import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.entity.Project;
import com.empresaTest.programa.entity.TypeInvestor;
import com.empresaTest.programa.model.CustomException;
import com.empresaTest.programa.model.InvestmentRequestDto;
import com.empresaTest.programa.repository.InvestmentRepository;
import com.empresaTest.programa.repository.InvestorRepository;
import com.empresaTest.programa.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class InvestmentServiceImpl implements InvestmentService{

    @Autowired
    private InvestmentRepository investmentRepository;

    @Autowired
    private InvestorRepository investorRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Investment saveInvestment(InvestmentRequestDto investmentDto){
        // buscar invesionista, si existe
        Optional<Investor> investor = investorRepository.findById(investmentDto.getInvestorId());
        if (investor.isEmpty()){
            throw new CustomException(103,"Error 103","No existe el inversionista", HttpStatus.BAD_REQUEST);
        }
        // si el inversor es de tipo BASICO, la inversion no debe exceder el 20% de su sueldo
        if (investor.get().getTypeInvestor().equals(TypeInvestor.BASIC)){
            BigDecimal sueldoPermitido = new BigDecimal("20").multiply(investor.get().getMonthlySalary()).divide(new BigDecimal("100"), RoundingMode.HALF_UP);
            if (investmentDto.getAmount().compareTo(sueldoPermitido)==1){
                throw new CustomException(104,"Error 104","El monto a invertir es muy alto, solo puede invertir el 20% de su sueldo.", HttpStatus.BAD_REQUEST);
            }
        }
        // si el inversor es de tipo FULL, la inversion no debe exceder el 25% de su sueldo
        if (investor.get().getTypeInvestor().equals(TypeInvestor.FULL)){
            BigDecimal sueldoPermitido = new BigDecimal("25").multiply(investor.get().getMonthlySalary()).divide(new BigDecimal("100"), RoundingMode.HALF_UP);
            if (investmentDto.getAmount().compareTo(sueldoPermitido)==1){
                throw new CustomException(104,"Error 104","El monto a invertir es muy alto, solo puede invertir el 25% de su sueldo.", HttpStatus.BAD_REQUEST);
            }
        }

        // buscar el proyecto, si existe.
        Optional<Project> project = projectRepository.findById(investmentDto.getProjectId());
        if (project.isEmpty()){
            throw new CustomException(100,"Error 100","No existe el proyecto", HttpStatus.BAD_REQUEST);
        }
        // el proyecto no debe estar cerrado.
        if (!project.get().getStateProject()){
            throw new CustomException(101,"Error 101","El proyecto ya esta cerrado", HttpStatus.BAD_REQUEST);
        }
        // el proyecto no debe estar cerrado, o el monto no exeder su monto de financiacion
        if (getMontoInversion(investmentDto.getProjectId()).add(investmentDto.getAmount()).compareTo(project.get().getAmount())==1){
            throw new CustomException(105,"Error 105","El monto exede al monto necesario para la recaudacion", HttpStatus.BAD_REQUEST);
        }

        Investment investment = new Investment();
        investment.setInvestor(investor.get());
        investment.setProject(project.get());
        investment.setAmount(investmentDto.getAmount());
        investment.setTransactionDate(LocalDateTime.now());
        return investmentRepository.save(investment);
    }

    @Override
    public List<Investment> getAllInvestmentByProjectName(String proyectName) {
        return investmentRepository.findByProyectName(proyectName);
    }

    @Override
    public List<Investment> getAllInvestmentByProjectId(Long proyectid) {
        return investmentRepository.findByProyectId(proyectid);
    }

    private BigDecimal getMontoInversion(Long projectId){
        List<Investment> investments = investmentRepository.findByProyectId(projectId);
        BigDecimal suma=BigDecimal.ZERO;
        for(Investment investment:investments){
            suma = suma.add(investment.getAmount());
        }
        return suma;
    }

}
