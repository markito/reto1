package com.empresaTest.programa.service;

import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.entity.TypeInvestor;
import com.empresaTest.programa.repository.InvestorRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class InvestorServiceImpl implements InvestorService{

    @Autowired
    private InvestorRepository investorRepository;

    @Override
    public Optional<Investor> getInvestor(String projectName) {
        return investorRepository.findByDocument(projectName);
    }

    @Override
    public Optional<Investor> getInvestorId(Long id) {
        return investorRepository.findById(id);
    }

    @Override
    public List<Investor> getAllInvestor() {
        return investorRepository.findAll();
    }

    @Override
    public Investor saveInvestor(Investor investor) {
        if (investor.getTypeInvestor().equals(TypeInvestor.BASIC)){
            investor.setMonthlySalary(new BigDecimal("4000000"));
        }else{
            investor.setMonthlySalary(new BigDecimal("8500000"));
        }
        return investorRepository.save(investor);
    }

    @Override
    public Investor updateInvestor(Long id, Investor investor) {
        Optional<Investor> investorOld = investorRepository.findById(id);
        if (investorOld.isPresent()){
            investorOld.get().setFirstName(investor.getFirstName());
            investorOld.get().setLastName(investor.getLastName());
            investorOld.get().setPhone(investor.getPhone());
            investorOld.get().setAddress(investor.getAddress());
            investorOld.get().setCityResidence(investor.getCityResidence());
            investorOld.get().setDocument(investor.getDocument());
            investorOld.get().setTypeInvestor(investor.getTypeInvestor());
            investorOld.get().setMonthlySalary(investor.getMonthlySalary());
            investorRepository.save(investorOld.get());
        }
        return investorOld.get();
    }

    @Override
    public void deleteInvestor(Long id) {
        investorRepository.deleteById(id);
    }
}
