package com.empresaTest.programa.service;

import com.empresaTest.programa.entity.Investor;
import com.empresaTest.programa.entity.Project;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface ProjectService {

    // obtener un proyecto
    Optional<Project> getProjectId(Long id);
    // obtener un proyecto
    Optional<Project> getProject(String projectName);

    // obtener todos los proyectos
    List<Project> getAllProject();

    // guardar nuevo proyecto
    Project saveProject(Project project);

    // Editar proyecto
    Project updateProject(Long id, Project project);

    // Eliminar proyecto
    void deleteProject(Long id);

}
