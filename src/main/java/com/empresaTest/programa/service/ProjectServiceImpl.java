package com.empresaTest.programa.service;

import com.empresaTest.programa.entity.Project;
import com.empresaTest.programa.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class ProjectServiceImpl implements ProjectService{

    @Autowired
    private ProjectRepository projectRepository;
    @Override
    public Optional<Project> getProjectId(Long id) {
        return projectRepository.findById(id);
    }

    @Override
    public Optional<Project> getProject(String projectName) {
        return projectRepository.findByProjectName(projectName);
    }

    @Override
    public List<Project> getAllProject() {
        return projectRepository.findAll();
    }

    @Override
    public Project saveProject(Project project) {
        // el proyecto debe tener 3000000 como cantidad a recaudar
        project.setAmount(new BigDecimal("3000000"));
        return projectRepository.save(project);
    }

    @Override
    public Project updateProject(Long id, Project project) {
        BigDecimal montoProyecto = new BigDecimal("3000000");
        Optional<Project> projectOld = projectRepository.findById(id);
        if (projectOld.isPresent()){
            projectOld.get().setProjectName(project.getProjectName());
            projectOld.get().setContactName(project.getContactName());
            projectOld.get().setPhone(project.getPhone());
            projectOld.get().setAddress(project.getAddress());
            projectOld.get().setDescription(project.getDescription());
            projectOld.get().setAmount(montoProyecto);
            projectOld.get().setStateProject(project.getStateProject());
            projectRepository.save(projectOld.get());
        }
        return projectOld.get();
    }

    @Override
    public void deleteProject(Long id) {
        projectRepository.deleteById(id);
    }
}
